#!/usr/bin/env node
import chalk from 'chalk'
import { exec } from 'child_process'
import { filter, find, maxBy } from 'lodash-es'
import fetch from 'node-fetch'
import ora from 'ora'
import semver from 'semver'
import stripAnsi from 'strip-ansi'
import table from 'text-table'

/***********
 * METHODS *
 ***********/

// Get result of commands
// https://github.com/npm/rfcs/issues/473#issuecomment-942645157
// exec('npm outdated') exits with code 1 (failure), so we need to get stdout with a specific function
function execAsync(command) {
  return new Promise(function (resolve, reject) {
    exec(command, (error, stdout, stderr) => {
      if (stderr !== '') reject(stderr)
      else resolve(stdout)
    })
  })
}

async function getNodeVersions() {
  // Get installed version
  const localNode = await execAsync('node --version')
  const localMajor = semver.major(semver.clean(localNode))

  // Get versions (already ordered by newer first)
  const resp = await fetch('https://nodejs.org/download/release/index.json')
  const respJson = await resp.json()
  if (resp.status >= 400) throw respJson.message

  // Filter to list only LTS
  const nodeLts = respJson.filter((v) => !!v.lts)
  let wantedLts = find(nodeLts, (v) => v.version.indexOf(`v${localMajor}`) !== -1)
  // No LTS version installed, check major-1
  if (wantedLts === undefined) wantedLts = find(nodeLts, (v) => v.version.indexOf(`v${localMajor - 1}`) !== -1)
  const latestLts = nodeLts[0]

  return [
    {
      name: 'node',
      update: semver.diff(semver.clean(localNode), semver.clean(latestLts.version)) || 'none',
      current: semver.clean(localNode),
      wanted: semver.clean(wantedLts.version),
      latest: semver.clean(latestLts.version)
    }
  ]
}

async function getNpmVersion() {
  // Get installed version
  const localNpm = await execAsync('npm --version')

  return semver.clean(localNpm)
}

// Execute a command a format data as array
async function execAndFormatDeps(command) {
  // Get outdated dependencies
  const stdout = await execAsync(command)
  const outdated = JSON.parse(stdout)

  // All dependencies
  // const outdated = JSON.parse(fs.readFileSync(OUTDATED_FILE))
  const outdatedArray = Object.entries(outdated)
    .map((p) => ({ ...p[1], name: p[0] }))
    .map((p) => {
      if (!p.current) return { ...p, update: 'missing', current: '' }
      return { ...p, update: semver.diff(p.current, p.latest) }
    })
  // console.log(outdatedArray)

  return outdatedArray
}

// Calculate max length to display all table with the same column width
function calculateMaxLength(array = []) {
  return {
    name: maxBy(array, (p) => p.name.length).name.length,
    update: maxBy(array, (p) => p.update.length).update.length,
    current: maxBy(array, (p) => p.current.length).current.length,
    wanted: maxBy(array, (p) => p.wanted.length).wanted.length,
    latest: maxBy(array, (p) => p.latest.length).latest.length
  }
}

// Color version
// current = wanted != latest > green green yellow
// current != wanted != latest > white white white
function colorVersion(pkg, attribute, maxLength) {
  if (pkg.current === pkg.wanted && pkg.wanted !== pkg.latest) {
    if (attribute === 'latest') return chalk.yellow(pkg[attribute].padEnd(maxLength[attribute] + 2, ' '))
    else return chalk.green(pkg[attribute].padEnd(maxLength[attribute] + 2, ' '))
  } else if (pkg.current !== pkg.wanted && pkg.wanted === pkg.latest) {
    if (attribute === 'current') return pkg[attribute].padEnd(maxLength[attribute] + 2, ' ')
    else return chalk.green(pkg[attribute].padEnd(maxLength[attribute] + 2, ' '))
    // } else if (pkg.current !== pkg.wanted && pkg.wanted !== pkg.latest) {
  } else {
    return pkg[attribute].padEnd(maxLength[attribute] + 2, ' ')
  }
}

// Color update type
function colorUpdate(updateType, maxLength) {
  if (updateType === 'patch') return chalk.green(updateType.padEnd(maxLength.update + 2, ' '))
  if (updateType === 'minor') return chalk.yellow(updateType.padEnd(maxLength.update + 2, ' '))
  if (updateType === 'major') return chalk.red(updateType.padEnd(maxLength.update + 2, ' '))
  else return updateType.padEnd(maxLength.update + 2, ' ')
}

// Print dependencies table by update type
function printDependency(deps = [], maxLength) {
  if (deps.length === 0 || (deps.length === 1 && deps.filter((d) => d.update === 'none').length)) {
    console.log('No updates')
    return
  }

  const head = [
    chalk.underline('Package'),
    chalk.underline('Update'),
    chalk.underline('Current'),
    chalk.underline('Wanted'),
    chalk.underline('Latest')
  ]
  const depsArray = deps.map((p) => [
    p.name.padEnd(maxLength.name + 2, ' '),
    colorUpdate(p.update, maxLength),
    colorVersion(p, 'current', maxLength),
    colorVersion(p, 'wanted', maxLength),
    colorVersion(p, 'latest', maxLength)
  ])

  console.log(table([head, ...depsArray], { stringLength: (s) => stripAnsi(s).length }))
}

/********
 * MAIN *
 ********/

async function main() {
  // Clear console
  console.clear()
  const spinner = ora('Getting outdated dependencies...').start()

  // Check node version
  const node = await getNodeVersions()
  // Get npm version
  const npm = await getNpmVersion()
  // Check global dependencies
  const globalDeps = await execAndFormatDeps('npm outdated --global --json --long')
  // Check local dependencies
  const localDeps = await execAndFormatDeps('npm outdated --json --long')

  const deps = [...node, ...globalDeps, ...localDeps]
  const maxLength = calculateMaxLength(deps)

  // Stop spinner and clear console
  spinner.stop()

  // Current node and npm versions
  console.log(chalk.cyan(`=== CURRENT VERSIONS`))
  console.log(`node     ${node[0].current}`)
  console.log(`npm      ${npm}`)
  console.log()

  // NodeJS
  console.log(chalk.cyan(`=== NODE (LTS)`))
  printDependency(node, maxLength)

  // Global dependencies
  const global = filter(deps, (p) => p.dependent === 'global')
  console.log(chalk.cyan(`\n=== GLOBAL DEPENDENCIES`))
  printDependency(global, maxLength)

  // Dependencies
  const dependencies = filter(deps, (p) => p.dependent !== 'global' && p.type === 'dependencies')
  console.log(chalk.cyan(`\n=== DEPENDENCIES`))
  printDependency(dependencies, maxLength)

  // Dev dependencies
  const devDependencies = filter(deps, (p) => p.dependent !== 'global' && p.type === 'devDependencies')
  console.log(chalk.cyan(`\n=== DEV DEPENDENCIES`))
  printDependency(devDependencies, maxLength)
}

main()
