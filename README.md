# check-deps

Custom script to list global and local dependencies et check if a new version is available

## Usage with NPX (recommended)

```bash
npx --yes gitlab:Finrod/check-deps
```

## Usage with installation / Development

```bash
git clone git@gitlab.com:Finrod/check-deps.git
cd check-deps
npm install

node check-deps.mjs
```
